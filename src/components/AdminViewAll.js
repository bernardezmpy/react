//Admin View All Products (Parent)

import { useEffect, useState } from "react"
import AdminViewAllMod from "./AdminViewAllMod";
import {Fragment} from 'react';

const AdminViewAll = () => {
  const [products, setProducts] = useState([]);

  useEffect(() => {

    fetch(`${process.env.REACT_APP_URI}/products/allProducts`, {
        headers: {
            'Content-Type' : 'application/json',
            Authorization: `Bearer ${localStorage.getItem('token')}`
        },
    })
    .then(response => response.json())
    .then(data => {
        console.log(data);

        setProducts(data.map(product => {
            return (<AdminViewAllMod key={product._id} product={product}/>)
        }))
    })

  }, []);

  return (
    <Fragment>
        {products}
    </Fragment>
  )
}

export default AdminViewAll