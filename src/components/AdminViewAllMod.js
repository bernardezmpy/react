// Admin View All Model

import {Container, Row, Col, Card, Button} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext';
import {Link} from 'react-router-dom';

export default function AdminViewAllMod({product}) {
	console.log(product)

	const {_id, category, image, name, description, price, stocks} = product;

	const [endUsers, setEndUsers] = useState(0);
	const [stocksAvailable, setStocksAvailable] = useState(0)
	const [isAvailable, setIsAvailable] = useState(true)

	const {user} = useContext(UserContext);


	useEffect(()=>{
		if(stocksAvailable === 0){
		setIsAvailable(false)
		}

	}, [stocksAvailable])


	function purchase(){

		if(stocksAvailable === 1){
			alert("Congratulations!")
		} 
		
		setEndUsers(endUsers + 1)
		setStocksAvailable(stocksAvailable - 1)
	}


	return (
		<Container>
			<Row className= 'my-5'>
				<Col xs={12} md={4} className='offset-md-4 offset-0'>
					<Card className="bg-light p-2 text-dark bg-opacity-50">
					      <Card.Body>
					      	<img src={image} class="img-fluid" alt=""/>
					        <h4>{name}</h4>
					        <h5>PHP {price}</h5>
					        <hr></hr>
					        {
					        	(user !== null) ?
					        	<Button as = {Link} to = {`/viewSpecific/${_id}`} variant="outline-light" className="yellow1 fw-bold text-dark shadow-sm" disabled={!isAvailable}>Details</Button>
					        	:
					        	<Button as={Link} to="/login" variant="primary"  disabled={!isAvailable}>CheckOut</Button>
					        }
					      </Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
		
		)
}

