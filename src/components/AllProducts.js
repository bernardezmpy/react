//Admin Dashboard Page: Retrieve all products (Child)

import {Container, Row, Col, Card, Button} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext';
import {Link} from 'react-router-dom';

export default function AllProducts({productsProp}) {
	console.log(productsProp)

	const {_id, category, name, description, price, stocks} = productsProp;

	const [endUsers, setEndUsers] = useState(0);
	const [stocksAvailable, setStocksAvailable] = useState(0)
	const [isAvailable, setIsAvailable] = useState(true)

	const {user} = useContext(UserContext);


	useEffect(()=>{
		if(stocksAvailable === 0){
		setIsAvailable(false)
		}

	}, [stocksAvailable])


	function purchase(){

		if(stocksAvailable === 1){
			alert("Congratulations!")
		} 
		
		setEndUsers(endUsers + 1)
		setStocksAvailable(stocksAvailable - 1)
	}


	return (
		<Container>
			<Row className= 'mt-3'>
				<Col xs={12} md={4} className='offset-md-4 offset-0'>
					<Card>
					      <Card.Body>
					        <Card.Title>{name}</Card.Title>
					        <hr></hr>
					        <Card.Subtitle>PHP {price}</Card.Subtitle>
					        {
					        	(user !== null) ?
					        	<Button as = {Link} to = {`/products/${_id}`} variant="primary"  disabled={!isAvailable}>Details</Button>
					        	:
					        	<Button as={Link} to="/login" variant="primary"  disabled={!isAvailable}>CheckOut</Button>
					        }
					      </Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
		
		)
}

