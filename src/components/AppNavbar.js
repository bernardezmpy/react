import {useContext, Fragment} from 'react';
import {Container, Nav, Navbar, Dropdown} from 'react-bootstrap';
import {NavLink} from 'react-router-dom';
import UserContext from '../UserContext';
import logo from '../images/logo1.png';
import adminLogo from '../images/logo3.png';
import cartlogo from '../images/cart.png';
import Image from 'react-bootstrap/Image'

export default function AppNavbar(){

	const {user} = useContext(UserContext);
	console.log(user.id)
	console.log(user.isAdmin)

	return(
		<Navbar className = "vw-100 yellow1 text-dark fw-bold shadow-lg" expnad="lg">
        <Container fluid>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
	          <Navbar.Collapse id="basic-navbar-nav">
		          <Nav className="navbar navbar-expand-lg d-flex">

		            {
		            	(user.id !== null)
		            	? (user.isAdmin === true)
		            	? <Fragment>
				            	<div>
				            		<Navbar.Brand as = {NavLink} to = "/adminDashboard">
		            					<img src={adminLogo} className="adminLogo branding" fluid="true" alt="Logo" />
		            				</Navbar.Brand >
		            			</div>	
		            			<div className="ms-auto navbar-nav">
		            					<Nav.Link as = {NavLink} to = "/adminRetrieveAll">Products</Nav.Link>
		            			 		<Nav.Link as = {NavLink} to = "/adminCreate">Create</Nav.Link>
		            					<Nav.Link as = {NavLink} to = "/orders">Orders</Nav.Link>
		            					<Nav.Link as = {NavLink} to = "/logout">Logout</Nav.Link>
				            	</div>
		            		</Fragment>

		            	: <Fragment>
				            	<div>
				            		<Navbar.Brand as = {NavLink} to = "/" className="">
		            					<img src={logo} className="brandlogo branding" alt="Logo" />
		            			</Navbar.Brand>
				            	</div>
		            			<div className="navbar-nav col-1">
		            				 	<Nav.Link as = {NavLink} to = "/" className="px-3 pt-3">Home</Nav.Link>
		            				 	<Nav.Link as = {NavLink} to = "/adminRetrieveAll" className="px-3 pt-3">Menu</Nav.Link>
		            				 	<Nav.Link as = {NavLink} to = "/logout" className="px-3 pt-3">Logout</Nav.Link>
		            					<Nav.Link as = {NavLink} to = "/cart" className="ps-3">
		            						<img src={cartlogo} width='35' height='35' className="addToCart" alt="Logo" />
		            					</Nav.Link>
		            			</div>
			           		</Fragment>

			           	: <Fragment>
			           			<Navbar.Brand as = {NavLink} to = "/" className="navbar-brand me-auto">
		            					<img src={logo} className="brandlogo branding" fluid="true" alt="Logo" />
		            			</Navbar.Brand>
		            			<div className="d-flex justify-content-end">
		            				<Nav.Link as = {NavLink} to = "/">Home</Nav.Link>
			           				<Nav.Link as = {NavLink} to = "/adminRetrieveAll">Menu</Nav.Link>
			           				<Nav.Link as = {NavLink} to = "/signup">Sign Up</Nav.Link>
				           			<Nav.Link as = {NavLink} to = "/login">Log In</Nav.Link>
		            			</div>
			           		</Fragment>

		            }

		          </Nav>
		      	</Navbar.Collapse>

        </Container>
      </Navbar>

		)
}