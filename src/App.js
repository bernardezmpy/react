import {BrowserRouter as Router, Routes, Route, Navigate} from 'react-router-dom';
import {useState, useEffect} from 'react';
import {UserProvider} from './UserContext'

import AppNavbar from './components/AppNavbar';
import AdminViewAll from './components/AdminViewAll';

import Home from './pages/Home';
import SignUp from './pages/SignUp';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error404 from './pages/Error404';
import ViewActive from './pages/ViewActive';
import ViewSpecific from './pages/ViewSpecific';

import CreateProduct from './pages/CreateProduct';
import UpdateProduct from './pages/UpdateProduct';
import Activate from './pages/Activate';
import Deactivate from './pages/Deactivate';

import AdminDashboard from './pages/AdminDashboard'

import './App.css';


function App() {

  const [user, setUser] = useState({id: null, isAdmin: false});
  const unsetUser = () => {
    localStorage.removeItem("token");
  }

  /*useEffect(() => {
    console.log(user)
  }, [user])*/

  useEffect(() => {
    fetch(`${process.env.REACT_APP_URI}/user/profile`, {
      headers: {Authorization: `Bearer ${localStorage.getItem("token")}`}
    })
      .then(response => response.json())
      .then(data => {
        console.log(data)

        setUser({id: data._id, isAdmin: data.isAdmin})
      })
  }, [])


  return (
    <UserProvider value = {{user, setUser, unsetUser}}>
      <Router>
          <AppNavbar/>
            <Routes>
              <Route path = "/" element = {<Home/>}/>
              <Route path = "/signup" element = {user.id !== null ? <Navigate to = "/" />: <SignUp/>}/>
              <Route path = "/login" element = {<Login/>}/>
              <Route path = "/logout" element = {<Logout/>}/>

              <Route path = "/viewProducts" element = {<ViewActive/>}/>
              <Route path = "/viewSpecific" element = {<ViewSpecific/>}/>
              
              <Route path = "/adminDashboard" element = {<AdminDashboard/>}/>
              <Route path = "/adminCreate" element = {<CreateProduct/>}/>
              <Route path = "/adminRetrieveAll" element = {<AdminViewAll/>}/>
              <Route path = "/adminUpdate" element = {<UpdateProduct/>}/>
              <Route path = "/adminActivate/:productId" element = {<Activate/>}/>
              <Route path = "/adminDeactivate/:productId" element = {<Deactivate/>}/>

              <Route path = "*" element = {<Error404/>}/>

            </Routes>
      </Router>
    </UserProvider>
  );
}

export default App;
