// Home Page: For Home => Navbar, Admin: Admin Dashboard, User: Banner, Menu/All Products

import {Container} from 'react-bootstrap';
import React, {useContext} from 'react';
import UserContext from '../UserContext';

import AppNavbar from '../components/AppNavbar';
import AdminViewAll from '../components/AdminViewAll';
import Banner from '../components/Banner';

export default function Home(){

	const {user} = useContext(UserContext);

	return(
		<Container>
			{
		        (user.isAdmin === true)
		        ? <AdminViewAll/>
		        : <Banner />
		    }
		</Container>
		)
}