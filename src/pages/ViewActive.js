//User Products Catalog Page: View all active product (Parent)

import {Fragment, useEffect, useState} from "react";
import AllProducts from "../components/AllProducts";

export default function ViewActive() {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_URI}/products/allActiveProducts`)
      .then((response) => response.json())
      .then((data) => {
        setProducts(
          data.map((product) => {
            return <AllProducts key={product._id} productsProp={product} />;
          })
        );
      });
  });
  return (<Fragment>{products}</Fragment>);
}
