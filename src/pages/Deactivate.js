//Admin Dashboard Page: Deactivate product

import {useParams, Navigate} from "react-router-dom";
import {useEffect, useNavigate} from "react";
import Swal from "sweetalert2";

export default function Deactivate() {
  
  const {productId} = useParams();
  const {navigate} = useNavigate();

  useEffect(() => {
    fetch(`${process.env.REACT_APP_URI}/products/${productId}/archived`, {
      method: "PATCH",
      headers: {Authorization: `Bearer ${localStorage.getItem("token")}`},
    })
      .then((response) => response.json())
      .then((data) => {
        console.log(data);

        if (data.archievedProduct) {
          Swal.fire({
            title: "Product Successfully Archived",
            icon: "success",
            text: "Product deactivated",
          });

          navigate("/")
        }

        else {
          Swal.fire({
            title: "Product Archived Failed",
            icon: "error",
            text: "Failure to deactivate the product, please try again.",
          });
        }
      });
  });
  return <Navigate to="/adminRetrieveAll" />;
}
