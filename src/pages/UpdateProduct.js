//Admin Dashboard Page: Update Product information

import {Button, Col, Row, Container} from "react-bootstrap";
import {useState, useEffect, useContext} from "react";
import {useParams} from "react-router-dom";
import Form from "react-bootstrap/Form";
import Swal from "sweetalert2";
import UserContext from "../UserContext";


export default function UpdateProduct() {
  const [category, setCategory] = useState("");
  const [image, setImage] = useState("");
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState("");
  const [stocks, setStocks] = useState("");
  const [isActive, setIsActive] = useState(true);
  const {productId} = useParams();
  
  const {user} = useContext(UserContext);


  useEffect(() => {
    if (
      category !== "" && image !== "" && name !== "" && description !== "" && price !== "" && stocks !== ""){
      setIsActive(false);
    } else {
      setIsActive(true);
    }
  }, [category, image, name, description, price, stocks]);


  function updateProduct(event) {
    event.preventDefault();

    fetch(`${process.env.REACT_APP_URI}/updateProduct/${productId}`, {
      method: "PUT",
      headers: {"Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`
      },
      body: JSON.stringify({
        category: category,
        image: image,
        name: name,
        description: description,
        price: price,
        stocks: stocks
      }),
    })
      .then((response) => response.json())
      .then((data) => {
        console.log(data);

        if (data.productExists) {
          Swal.fire({
            title: "Product Successfully Updated",
            icon: "success",
            text: "Product information has been updated",
          });

        }

        else {
          Swal.fire({
            title: "Update Failed",
            icon: "error",
            text: "Please check if the information you input are correct.",
          });

        }
      });
  }

  return (
    <Container className = "my-4">
      <Row className="mt-5">
        <Col className = "col-md-5 col-8 offset-md-4 offset-2">
          <Form onSubmit={updateProduct} className="p-3">
            
            <Form.Group controlId = "productCategory">
                  <Form.Label className = "fw-bold">Category</Form.Label>
                  <Form.Control
                    className = "mb-3"
                    type = "text"
                    placeholder = "Update Product Category"
                    value = {category}
                    onChange = {event => setCategory(event.target.value)}
                    required/>
                </Form.Group>

                <Form.Group controlId = "productName">
                  <Form.Label className = "fw-bold">Name</Form.Label>
                  <Form.Control
                    className = "mb-3"
                    type = "text"
                    placeholder = "Update Product Name"
                    value = {name}
                    onChange = {event => setName(event.target.value)}
                    required/>
                </Form.Group>

                <Form.Group controlId = "productPrice">
                  <Form.Label className = "fw-bold">Price</Form.Label>
                  <Form.Control
                    className = "mb-3"
                    type = "number"
                    placeholder = "Update Price"
                    value = {price}
                    onChange = {event => setPrice(event.target.value)}
                    required/>
                </Form.Group>

                <Form.Group controlId = "productStock">
                  <Form.Label className = "fw-bold">Count in Stock</Form.Label>
                  <Form.Control
                    className = "mb-3"
                    type = "number"
                    placeholder = "Update Stocks"
                    value = {stocks}
                    onChange = {event => setStocks(event.target.value)}
                    required/>
                </Form.Group>

                <Form.Group controlId = "productDescription">
                  <Form.Label className = "fw-bold">Description</Form.Label>
                  <Form.Control
                    className = "mb-3"
                    type = "text"
                    placeholder = "Update Description"
                    value = {description}
                    onChange = {event => setDescription(event.target.value)}
                    required/>
                </Form.Group>

                <Form.Group controlId = "productImage">
                  <Form.Label className = "fw-bold">Image</Form.Label>
                  <Form.Control
                    className = "mb-3"
                    type = "url"
                    placeholder = "Update Image URL"
                    value = {image}
                    onChange = {event => setImage(event.target.value)}
                    required/>
                </Form.Group>

            <Button
                  className = "primary"
                  type = "submit">
                  Update Product
                </Button>
          </Form>
        </Col>
      </Row>
    </Container>
  );
}
