//Logout Page: Logout Admin and User

import {Navigate} from 'react-router-dom';
import UserContext from '../UserContext';
import {useContext, useEffect} from 'react';
import Swal from 'sweetalert2';

export default function Logout(){

	const{setUser, unsetUser} = useContext(UserContext);
	
	unsetUser();

	useEffect(() => {
		setUser({id: null, isAdmin: false})
	}, [])

	Swal.fire({
		title: "Thank you!",
		icon: "info",
		text: "Hope to serve you again soon"
		
	})

	return(
		<Navigate to = "/login" />
		)
}