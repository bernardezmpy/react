import {Button, Card, Container, Row, Col} from "react-bootstrap";
import {useEffect, useState} from "react";
import {useParams} from "react-router-dom";

export default function SpecificProduct() {
  const [category, setCategory] = useState("");
  const [image, setImage] = useState("");
  const [name, setName] = useState("");
  const [description, setDescription] = useState(0);
  const [price, setPrice] = useState("");
  const [stocks, setStocks] = useState(0);
  
  const {productId} = useParams();


  useEffect(() => {
    fetch(`${process.env.REACT_APP_URI}/products/${productId}`)
      .then((response) => response.json())
      .then(
        (data) => {
          setCategory(data.category);
          setImage(data.image);
          setName(data.name);
          setDescription(data.description);
          setPrice(data.price);
          setStocks(data.stocks);
        },
        [productId]
      );
  });

  return (
    <Container>
      <Row>
        <Col className="col-8 offset-2 mt-5">
          <Card style={{ width: '18rem' }}>
              <Card.Img variant="top" src="holder.js/100px180" />
                <Card.Body>
                  <Card.Title>{name}</Card.Title>
                  <Card.Text>{description}</Card.Text>
                  <Card.Text>{price}</Card.Text>
                  <Button variant="primary">Order</Button>
              </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
    
  );
}
